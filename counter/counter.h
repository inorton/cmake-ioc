#ifndef FOO_COUNTER_H
#define FOO_COUNTER_H

#include <stddef.h>

int get_count(void);

#endif