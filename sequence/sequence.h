//
// Created by inb on 07/11/18.
//

#ifndef CMAKE_IOC_FOO_H
#define CMAKE_IOC_FOO_H

#include <stddef.h>

#include "counter.h"

extern int get_count(void);

void sequence_reset(void);
int sequence_next(void);
int sequence_value(void);

#endif //CMAKE_IOC_FOO_H
