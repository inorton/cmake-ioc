//
// Created by inb on 07/11/18.
//

#include "sequence.h"
#include <stdio.h>
#include <string.h>

static int value = 0;

void sequence_reset(void) {
    value = 0;
}

int sequence_next(void) {
    value = value + get_count();
    return value;
}

int sequence_value(void) {
    return value;
}