//
// Created by inb on 07/11/18.
//

#include "randreader.h"
#include <stdio.h>

unsigned char random_byte(void) {
    FILE* r = fopen("/dev/urandom", "r");
    int value = fgetc(r);
    fclose(r);
    return (unsigned char) value;
}