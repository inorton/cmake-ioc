add_executable(test-big test.c)
target_link_libraries(test-big PRIVATE sequence-big)

add_executable(test-small test.c)
target_link_libraries(test-small PRIVATE sequence-small)

add_executable(test-rand test.c)
target_link_libraries(test-rand PRIVATE sequence-rand)