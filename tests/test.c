//
// Created by inb on 07/11/18.
//

#include <stdio.h>
#include "sequence.h"

int main(int argc, char** argv) {
    sequence_reset();
    printf("value = %d\n", sequence_value());
    printf("next = %d\n", sequence_next());
    printf("next = %d\n", sequence_next());
    sequence_reset();
    printf("value = %d\n", sequence_value());
    printf("next = %d\n", sequence_next());

    return 0;
}

